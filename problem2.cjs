const fs = require('fs');
const path = require('path');

const dataConvertedUpperCase = path.join(__dirname, './to-upper-case.txt');
const dataConvertedLowerCase = path.join(__dirname, './to-lower-case.txt');
const dataOfSortFile = path.join(__dirname, './sorted-file.txt');
const appendFilePath = path.join(__dirname, './filenames.txt');

function multipleScenarios(filesFolder) {
    return new Promise((resolve, reject) => {
        readFilePath(filesFolder)
            .then((data) => {
                return dataToUpperCase(data);
            })
            .then((data) => {
                return lowerCaseToSplitData(data);
            })
            .then((data) => {
                return sortedDataFile(data);
            })
            .then((message) => {
                console.log(message);
                return deleteAllFiles(appendFilePath);
            })
            .then((message) => {
                console.log(message);
                resolve("Operations are done");
            })
            .catch((error) => {
                if (error) {
                    reject(new Error(error));
                }
            })
    });
}

function readFilePath(filesFolder) {
    return new Promise((resolve, reject) => {
        fs.readFile(filesFolder, 'utf-8', (error, data) => {
            if (error) {
                reject(new Error(error));
            } else {
                console.log("File read successfully");
                resolve(data);
            }
        });
    });
}

function dataToUpperCase(data) {
    return new Promise((resolve, reject) => {
        const convertUpperCase = data.toUpperCase();
        fs.writeFile(dataConvertedUpperCase, convertUpperCase.toString(), (error) => {
            if (error) {
                reject(new Error(error));
            } else {
                console.log('Text file converted to uppercase');
                storeToNewFile(dataConvertedUpperCase + '\n')
                    .then((message) => {
                        console.log(message);
                    })
                    .catch((error) => {
                        console.error(error);
                    })
                resolve(data);
            }
        });
    });

}

function storeToNewFile(data, appendFilePath = path.join(__dirname, './filenames.txt')) {
    return new Promise((resolve, reject) => {
        fs.appendFile(appendFilePath, data.toString(), (error) => {
            if (error) {
                reject(new Error(error));
            } else {
                resolve("File name is stored to new filenames.txt");
            }
        });
    });
}

function lowerCaseToSplitData(data) {
    return new Promise((resolve, reject) => {
        const convertLowerCase = data.toLowerCase();
        const splitData = convertLowerCase.split('.').join('.\n');
        fs.writeFile(dataConvertedLowerCase, splitData.toString(), (error) => {
            if (error) {
                reject(new Error(error));
            } else {
                console.log("Text converted to Lower case and splitted into sentences");
                storeToNewFile(dataConvertedLowerCase + '\n')
                    .then((message) => {
                        console.log(message);
                    })
                    .catch((error) => {
                        console.error(error);
                    })
                resolve(splitData);
            }
        });
    });
}


function sortedDataFile(data) {
    return new Promise((resolve, reject) => {
        const sortData = data.split('\n').sort();
        fs.writeFile(dataOfSortFile, sortData.toString(), (error) => {
            if (error) {
                reject(new Error(error));
            } else {
                storeToNewFile(dataOfSortFile + '\n')
                    .then((message) => {
                        console.log(message);
                    })
                    .catch((error) => {
                        console.error(error);
                    })
                resolve('Sorted successfully');
            }
        });
    });
}

function deleteAllFiles(appendFilePath) {
    return new Promise((resolve, reject) => {
        fs.readFile(appendFilePath, 'utf-8', (error, data) => {
            if (error) {
                reject(new Error(error));
            } else {
                const fileNamesToDelete = data.split('\n').map((file) => {
                    return deleteEachFile(file);
                })
                Promise.all(fileNamesToDelete)
                    .then((message) => {
                        console.log(message);
                        resolve('All files has been deleted successfully');
                    })
                    .catch((error) => {
                        console.error(error);
                    });
            }
        });
    });
}

function deleteEachFile(fileName) {
    // console.log(fileName);
    return new Promise((resolve, reject) => {
        fs.unlink(fileName, (error) => {
            if (error === false) {
                reject(new Error(error));
            } else {
                resolve(`${fileName} is deleted successfully`);
            }
        });
    });
}

module.exports = multipleScenarios;