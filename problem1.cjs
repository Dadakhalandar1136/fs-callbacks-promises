const fs = require('fs');

function createAndDeleteFiles(filesFolder) {
    return createDairectory(filesFolder)
        .then((filesFolder) => {
            return createRandomFiles(filesFolder);
        })
        .then((arrayOfFileNames) => {
            deleteRandomFiles(arrayOfFileNames);
        })
        .catch((error) => {
            if (error) {
                console.error(error);
            }
        })
}

function createDairectory(filesFolder) {
    return new Promise((resolve, reject) => {
        fs.mkdir(filesFolder, { recursive: true }, (error) => {
            if (error) {
                reject(new Error(error));
            } else {
                console.log("Directory created successfully");
                resolve(filesFolder);
            }
        });
    });
}

function createRandomFiles(filesFolder) {
    return new Promise((resolve, reject) => {
        const randomFiles = Math.round(Math.random() * 5);
        let arrayOfFileNames = Array(randomFiles).fill(0).map((element, index) => {
            return `${filesFolder}/file${index}.json`;
        });
        let randomCreatedFiles = arrayOfFileNames.map((fileNames, index) => {
            return createEachRandomFile(fileNames, index);
        })
        Promise.all(randomCreatedFiles)
            .then((createdFiles) => {
                console.log(createdFiles);
                console.log("files created successfully");
                resolve(arrayOfFileNames);
            })
            .catch((error) => {
                reject(new Error(error));
            })
    });
}

function createEachRandomFile(fileNames, index) {
    return new Promise((resolve, reject) => {
        fs.writeFile(fileNames, JSON.stringify({ name: 'Dadu', age: '25' }), (error) => {
            if (error) {
                reject(new Error(error));
            } else {
                resolve(`file${index}.json created successfully`)
            }
        });
    });
}

function deleteRandomFiles(arrayOfFileNames) {
    return new Promise((resolve, reject) => {
        let randomCreatedFiles = arrayOfFileNames.map((fileNames, index) => {
            return deleteEachRandomFile(fileNames, index);
        })
        Promise.all(randomCreatedFiles)
            .then((deletedFiles) => {
                console.log(deletedFiles);
                console.log("files deleted successfully");
                resolve();
            })
            .catch((error) => {
                reject(new Error(error));
            })
    });
}

function deleteEachRandomFile(fileNames, index) {
    return new Promise((resolve, reject) => {
        fs.unlink(fileNames, (error) => {
            if (error) {
                reject(new Error(error));
            } else {
                resolve(`file${index}.json deleted successfully`);
            }
        });
    });
}

module.exports = createAndDeleteFiles;
