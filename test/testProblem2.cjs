const multipleScenarios = require('../problem2.cjs');
const path = require('path');
const pathFile = path.join(__dirname, './lipsum.txt');

multipleScenarios(pathFile)
    .then((message) => {
        console.log(message);
    })
    .catch((error) => {
        console.error(error);
    })